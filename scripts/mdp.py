#!/usr/bin/env python
from copy import deepcopy
from read_config import read_config

#Read from JSON
config = read_config()
	
#Instance Variables
move_list = config['move_list']
map_size = config['map_size']
start = config['start']
goal = config['goal']
walls = config['walls']
pits = config['pits']

p_for = config['prob_move_forward']
p_back = config['prob_move_backward']
p_left = config['prob_move_left']
p_right = config['prob_move_right']

discount = config['discount_factor']
r_step = config['reward_for_each_step']
r_wall = config['reward_for_hitting_wall']
r_goal = config['reward_for_reaching_goal']
r_pit  = config['reward_for_falling_in_pit']

# convert state to str
def to_str(state):
    state_str = ''.join([str(x) for x in state])
    return state_str

def isTerminalState(state, goal, pits, walls):
	if (to_str(state) in to_str(goal)) or (state in pits) or (state in walls):
		return True
	return False

def getPossibleActions(state):
	poss_act = []
    	#N
	poss_act.append(("N", [state[0]-1 if state[0] > 0 else 0, state[1]] ))
	#S
	poss_act.append(("S",[state[0]+1 if state[0] < map_size[0] - 1 else state[0], state[1]]))
	#W
	poss_act.append(("W",[state[0], state[1]-1 if state[1] > 0 else 0]))
	#E
	poss_act.append(("E",[state[0], state[1]+1 if state[1] < map_size[1] - 1 else state[1]]))
	return poss_act

def bloop(state, action, nextState, mdp_map):
  	#N
	move = None
	if nextState[0] is "N":
		if action is "forward":
			move = [state[0]-1  if state[0] > 0 else 0 ,state[1]]
		if action is "backward":
			move = [state[0]+1 if state[0] < map_size[0] - 1 else state[0],state[1]]
		if action is "left":
			move = [state[0],state[1]-1  if state[1] > 0 else 0 ]
		if action is "right":
			move = [state[0],state[1]+1 if state[1] < map_size[1] - 1 else state[1]]

	if nextState[0] is "S":
		if action is "forward":
			move = [state[0]+1 if state[0] < map_size[0] - 1 else state[0],state[1]]
		if action is "backward":
			move = [state[0]-1  if state[0] > 0 else 0 ,state[1]]
		if action is "left":
			move = [state[0],state[1]+1 if state[1] < map_size[1] - 1 else state[1] ]
		if action is "right":
			move = [state[0],state[1]-1  if state[1] > 0 else 0 ]

	if nextState[0] is "W":
		if action is "forward":
			move = [state[0],state[1]-1  if state[1] > 0 else 0 ]
		if action is "backward":
			move = [state[0],state[1]+1 if state[1] < map_size[1] - 1 else state[1] ]
		if action is "left":
			move = [state[0]+1 if state[0] < map_size[0] - 1 else state[0],state[1]]
		if action is "right":
			move = [state[0]-1  if state[0] > 0 else 0 ,state[1]]

	if nextState[0] is "E":
		if action is "forward":
			move = [state[0],state[1]+1 if state[1] < map_size[1] - 1 else state[1]]
		if action is "backward":
			move = [state[0],state[1]-1  if state[1] > 0 else 0 ]
		if action is "left":
			move = [state[0]-1  if state[0] > 0 else 0 ,state[1]]
		if action is "right":
			move = [state[0]+1 if state[0] < map_size[0] - 1 else state[0],state[1]]
	
	return mdp_map[move[0]][move[1]]
	      

			
  	
        
def getReward(state, action, nextState):
	reward = 0
	reward += r_step
	
	#N
	if nextState[0] is "N":
		if action is "forward":
			move = [state[0]-1,state[1]]
		if action is "backward":
			move = [state[0]+1 ,state[1]]
		if action is "left":
			move = [state[0],state[1]-1]
		if action is "right":
			move = [state[0],state[1]+1]

	#S
	if nextState[0] is "S":
		if action is "forward":
			move = [state[0]+1,state[1]]
		if action is "backward":
			move = [state[0]-1,state[1]]
		if action is "left":
			move = [state[0],state[1]+1]
		if action is "right":
			move = [state[0],state[1]-1]

	#W
	if nextState[0] is "W":
		if action is "forward":
			move = [state[0],state[1]-1]
		if action is "backward":
			move = [state[0],state[1]+1]
		if action is "left":
			move = [state[0]+1,state[1]]
		if action is "right":
			move = [state[0]-1,state[1]]

	#E
	if nextState[0] is "E":
		if action is "forward":
			move = [state[0],state[1]+1]
		if action is "backward":
			move = [state[0],state[1]-1]
		if action is "left":
			move = [state[0]-1,state[1]]
		if action is "right":
			move = [state[0]+1,state[1]]

        # if moving into wall or out of bounds, reward = r_wall
	if move[0] < 0 or move[0] > map_size[0]-1 or move[1] < 0 or move[1] > map_size[1]-1 or move in walls:
              reward = r_wall

	if move in pits:
		reward += r_pit

	#if to_str(move) in to_str(goal):
	#	reward += r_goal

	return reward
	

def mdp(mdp_map, policy_map):
	possibleMoves = [("forward", p_for),("backward", p_back),("left", p_left),("right", p_right)]

	new_vals = []
	new_policy = []
	for row in range(map_size[0]):
	        new_policy.append([])
		for col in range(map_size[1]):
		        new_policy[row].append(policy_map[row][col])
	
 	#initialize the valueMap 
	for row in range(map_size[0]): 
		new_vals.append([]) 
		for col in range(map_size[1]): 
			new_vals[row].append(0);

        for row in range(map_size[0]):
		for col in range(map_size[1]):
			#update vals if not terminal
			if not isTerminalState([row,col], goal, pits, walls):
				newVal = None
				for next_state in getPossibleActions([row,col]):
					val = 0
					for action, prob in possibleMoves:
						val += prob*(getReward([row,col], action, next_state) + (discount * bloop([row,col], action, next_state, mdp_map) ));
						#print "SSSSSSssssssssssssssssssssssssssssssssssssssssssssssssssss"
						if(row == 0 and col == 2):
						 print prob
						 print mdp_map[row][col]
						 print getReward([row,col],action, next_state)
						 print(discount*mdp_map[row][col])
					newVal = val if newVal is None else max(newVal, val)
				new_vals[row][col] = newVal
			else:
			  if [row,col] in pits:
				new_vals[row][col] = r_pit
			  if [row, col] in walls:
			  	new_vals[row][col] = r_wall
			  if to_str([row, col]) in to_str(goal):
			        new_vals[row][col] = r_goal
			#print(new_vals)

	for row in range(map_size[0]):
		for col in range(map_size[1]):
			if not isTerminalState([row, col], goal, pits, walls):
				policy_tuple = (None, None)
				for pol, next_state in getPossibleActions([row,col]):
					if policy_tuple[1] is None:
						policy_tuple = (pol,new_vals[next_state[0]][next_state[1]])
					else:
						if policy_tuple[1] < new_vals[next_state[0]][next_state[1]]:
							policy_tuple = (pol, new_vals[next_state[0]][next_state[1]])
				new_policy[row][col] = policy_tuple[0]
				
					
	return new_vals, new_policy		

    	
