#!/usr/bin/env python
from copy import deepcopy
from read_config import read_config

config = read_config()

#Instance Variables

map_size = config['map_size']
const = 100000 
alpha = 0.5

p_for = config['prob_move_forward']
p_back = config['prob_move_backward']
p_left = config['prob_move_left']
p_right = config['prob_move_right']

walls = config['walls']
discount = config['discount_factor']
r_step = config['reward_for_each_step']

def find_best_index(lisst):
   #finds the sum of every element of list
   a= [(x[0]+const/x[1]) if x[1] != 0 else (x[0])for x in lisst]
   #returns the index of largest element 
   return [a.index(max(a)), max(a)]

def sum_probs(grid,x,y, direct):
   default = find_best_index(grid[y][x])
   if(direct == 'N'):
     #how to handle "walls"
     forward = find_best_index(grid[y-1][x])[1] if y != 0 else default[1]
     left =  find_best_index(grid[y][x-1])[1] if x != 0 else default[1]
     right = find_best_index(grid[y][x+1])[1] if x != map_size[1]-1 else default[1]
     backward = find_best_index(grid[y+1][x])[1] if y != map_size[0]-1 else default[1]
   elif(direct == 'S'):
     backward = find_best_index(grid[y-1][x])[1] if y != 0 else default[1]
     right =  find_best_index(grid[y][x-1])[1] if x != 0 else default[1]
     left = find_best_index(grid[y][x+1])[1] if x != map_size[1]-1 else default[1]
     forward = find_best_index(grid[y+1][x])[1] if y != map_size[0]-1 else default[1]
   elif(direct == 'W'):
     right = find_best_index(grid[y-1][x])[1] if y != 0 else default[1]
     forward =  find_best_index(grid[y][x-1])[1] if x != 0 else default[1]
     backward = find_best_index(grid[y][x+1])[1] if x != map_size[1]-1 else default[1]
     left = find_best_index(grid[y+1][x])[1] if y != map_size[0]-1 else default[1]
   elif(direct == 'E'):
     left = find_best_index(grid[y-1][x])[1] if y != 0 else default[1]
     backward =  find_best_index(grid[y][x-1])[1] if x != 0 else default[1]
     forward = find_best_index(grid[y][x+1])[1] if x != map_size[1]-1 else default[1]
     right = find_best_index(grid[y+1][x])[1] if y != map_size[0]-1 else default[1]
   else:
     print "we fucked"
     return None
   return p_for*forward + p_back*backward + p_left*left +p_right*right 
#find which direction we should go
def find_direction(x):
   if(x==0): 
      return 'N'
   elif(x==1):
      return 'S'
   elif(x==2):
      return 'W'
   elif(x==3):
      return 'E'
   else:
      return None;

def q_learn(grid):

    new_grid = deepcopy(grid)

    for y in range(map_size[0]):
        for x in range(map_size[1]):
            if [y,x] in walls:
                print "FUCK WALL"
                continue
	    #pick a direction
	    pair  = find_best_index(grid[y][x]) #returns (index, Q(s,a)+L(s,a))
	    index = pair[0]
            old_value = grid[y][x][index][0]; #get Q(s,a) without L(s,a)
            direction = find_direction(index)
 	    #value  =  sum_probs(grid,x, y, direction);
	    otherval = None
            if(direction == 'N'):
	      if(y == 0):
                otherval = pair[1]
              else:			
                otherval = find_best_index(grid[y-1][x])[1]
            elif(direction == 'S'):
	      if(y == map_size[0]-1):
                otherval = pair[1]
              else:			
                otherval = find_best_index(grid[y+1][x])[1]
            elif(direction == 'W'):
	      if(x == 0):
                otherval = pair[1]
              else:			
                otherval = find_best_index(grid[y][x-1])[1]
            elif(direction == 'E'):
	      if(x == map_size[1]-1):
                otherval = pair[1]
              else:			
                otherval = find_best_index(grid[y][x+1])[1]
            else:
              print "plz kill me"
            # TODO: do something for wall for imperfect motion
            q_new = (1-alpha)*old_value + alpha*(r_step + discount*otherval)
            new_grid[y][x][index][0] = q_new
            new_grid[y][x][index][1]+=1
            #print(otherval)
    return new_grid






